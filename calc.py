from tkinter import *
from tkinter import messagebox
from math import log10, ceil
from ipaddress import *

class Calculadora:
    def __init__(self, master):
        self.master = master
        master.title("Calculadora")
        self.mascara = ""
        self.prefijo = ""
        self.prefijoViejo = 0
        # Variables
        self.tipoIP = StringVar()
        self.mascaraRed = StringVar()
        self.nuevoSR = StringVar()
        self.nuevoHosts = StringVar()
        self.varMenu = StringVar()
        self.selec = IntVar()
        self.nuevoPrefijo = StringVar()
        self.tipoIP.set("Prefijo: ")
        self.mascaraRed.set("Súbmáscara de red: ")
        # Label
        self.etTipoIP = Label(master, textvariable=self.tipoIP)
        self.etMascaraRed = Label(master, textvariable=self.mascaraRed)
        self.etNumSR = Label(master, textvariable=self.nuevoSR)
        self.etNuevoP = Label(master, textvariable=self.nuevoPrefijo)
        self.etNuevoHosts = Label(master, textvariable=self.nuevoHosts)
        # Listbox
        self.lHosts = Listbox(master, selectmode=SINGLE)
        self.lSubredes = Listbox(master, selectmode=SINGLE)
        # Entry
        self.inIP = Entry(master, text="0.0.0.0")
        self.inNum = Entry(master, width=6)

        # Boton
        self.btEnter = Button(master, text="Calcular", command=lambda: self.switch(self.selec.get()))
        self.bNHosts = Radiobutton(master, variable=self.selec, value=0, text="Número de Hosts")
        self.bNSubredes = Radiobutton(master, variable=self.selec, value=1, text="Número de Subredes")
        self.bPrefijo = Radiobutton(master, variable=self.selec, value=2, text="Prefijo")
        # Bind
        self.inIP.bind("<KeyRelease>", lambda x: self.obtenerCaractIP())
        self.lSubredes.bind('<<ListboxSelect>>', self.actualizarListaHost)
        # Set & pack
        self.inIP.grid(row=0, column=0)
        self.inNum.grid(row=2, column=1)
        self.etTipoIP.grid(sticky=W, row=0, column=2)
        self.bNHosts.grid(sticky=W, row=1, column=0)
        self.bNSubredes.grid(sticky=W, row=2, column=0)
        self.bPrefijo.grid(sticky=W, row=3, column=0)
        self.etMascaraRed.grid(sticky=W, row=1, column=2)
        self.etNuevoP.grid(sticky=W, row=2, column=2)
        self.etNuevoHosts.grid(sticky=W, row=3, column=2)
        self.etNumSR.grid(sticky=W, row=4, column=2)
        self.lHosts.grid(row=9, column=2)
        self.lSubredes.grid(row=9, column=0)
        self.btEnter.grid(row=10, column=2)

    def switch(self, valor):
        if (valor == 0):
            self.dividirEnHosts()
        elif (valor == 1):
            self.dividirEnSubredes()
        else:
            self.dividirPrefijo()

    def obtenerCaractIP(self):
        ip = self.inIP.get()
        ipSplit = ip.split('.')
        try:
            oct = int(ipSplit[0])
        except(ValueError):
            oct = 0
        if (oct in range(1, 128)):
            self.tipoIP.set("Prefijo: A")
            self.mascara = "255.0.0.0"
        elif oct in range(128, 192):
            self.tipoIP.set("Prefijo: B")
            self.mascara = "255.255.0.0"
        elif oct in range(192, 223):
            self.tipoIP.set("Prefijo: C")
            self.mascara = "255.255.255.0"
        else:
            self.tipoIP.set("IP NO VALIDA")
        self.mascaraRed.set("Súbmáscara de red: " + self.mascara)
        self.prefijoViejo = self.mascara.count("255") * 8
        try:
            self.nuevoHosts.set("Número de Hosts: " + str( len(list(ip_network(self.inIP.get() + "/" + str(self.prefijoViejo)).hosts()))))
            self.nuevoSR.set("Número de Subredes: " + str( len([ip for ip in IPv4Network(self.inIP.get() + "/" + str(self.prefijoViejo))]) - 2 ))
        except:
            self.nuevoHosts.set("Número de Hosts: ")
            self.nuevoSR.set("Número de Subredes: ")

    def dividirEnHosts(self):
        num = int(self.inNum.get())
        hostsDisponibles = 2 ** (32 - self.prefijoViejo) - 2
        bitsNecesarios = ceil(log10(num + 2) / log10(2))

        if num > hostsDisponibles:
            messagebox.showerror("Error", "Error: Número invalido")
            return
        cadena = ""
        self.prefijo = 32 - bitsNecesarios
        self.actualizarNumeros()
        self.actualizarListaSR()
        return

    def actualizarListaSR(self):
        self.lSubredes.delete(0, END)
        ip = self.inIP.get()
        subredes = list(ip_network(ip + "/" + str(self.prefijoViejo)).subnets(new_prefix=self.prefijo))
        for sr in subredes:
            self.lSubredes.insert(END, str(sr))
        self.lSubredes.update_idletasks()
        return

    def actualizarListaHost(self,event):
        self.lHosts.delete(0, END)
        try:
            hosts = list(ip_network(self.lSubredes.get(self.lSubredes.curselection()[0])).hosts())
            for host in hosts:
                self.lHosts.insert(END, str(host))
            self.lHosts.update_idletasks()
        except:
            pass
        return

    def dividirEnSubredes(self):
        num = int(self.inNum.get())
        redesDisponibles = 2 ** (29 - self.prefijoViejo)
        bitsNecesarios = ceil(log10(num + 2) / log10(2))
        if num > redesDisponibles or bitsNecesarios + self.prefijoViejo > 29:
            messagebox.showerror("Error", "Error: Número invalido")
            return
        self.prefijo = self.prefijoViejo + bitsNecesarios
        self.actualizarNumeros()
        self.actualizarListaSR()
        return

    def dividirPrefijo(self):
        self.prefijo = int(self.inNum.get())
        if (self.prefijo >= 32 or self.prefijo < self.prefijoViejo):
            messagebox.showerror("Error", "Error: Número invalido")
        self.actualizarNumeros()
        self.actualizarListaSR()

        return

    def actualizarNumeros(self):
        bins = []
        for i in range(4):
            bins.append(''.join(["1" if (self.prefijo > j + 1 * (i * 8)) else "0" for j in range(8)]))
        self.nuevoPrefijo.set("Nueva máscara de red: " +
                              '.'.join([str(int(binario, 2)) for binario in bins])
                              + "/" + str(self.prefijo))
        self.nuevoHosts.set("Número de Hosts: " +
                            str( len(list(ip_network(self.inIP.get() + "/" + str(self.prefijo)).hosts()))))
        self.nuevoSR.set("Número de Subredes: " +
                         str( len( list(ip_network(self.inIP.get() + "/" + str(self.prefijoViejo)).subnets(new_prefix=self.prefijo)))))


root = Tk()
my_gui = Calculadora(root)
root.mainloop()
