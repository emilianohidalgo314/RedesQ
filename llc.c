#include <sys/socket.h>
#include <linux/if_packet.h>
#include <net/ethernet.h> /* the L2 protocols */
#include <stdlib.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <sys/time.h>
#include <stdbool.h>
#include <math.h>

int obtenerDatos(int ds);	//descriptor de socket
void imprimirTrama(unsigned char * trama, int tam);
void leerArchivo(char* nombreArchivo);
void analizarLLC(unsigned char * trama);
void analizarDSAP(int dsap);
void analizarSSAP(int ssap);
void switchSAP(int n);
bool esLLC(int ds, unsigned char* tramaR);
void escuchar(int ds,  int indice, unsigned char* tramaR);
void intACadenaBinaria(char* buffer, int num);
void analizarControl(int controlByte1, int controlByte2);
int cadenaBinariaAInt(char* cadena);


int main(){
    int packet_socket, indice, valorS;
    unsigned char tramaR[1514];    //tramaRecibida
    while(1) {
        printf("\n1.Abrir archivo\n2.Escaneo de red\n3.Salir\n");
        scanf("%d", &valorS);
        switch (valorS) {
            case 1:
                leerArchivo("/home/emiliano/Code/C/Redes/LLC/tramas.txt");
                break;
            case 2:
                //Abir socket
                if ( (packet_socket = socket(AF_PACKET, SOCK_PACKET, htons(ETH_P_ALL))) == -1 ){
                    perror("Woops");
                    exit(0);
                }
                perror("Socket abierto con éxito");
                indice = obtenerDatos(packet_socket);
                escuchar(packet_socket,indice,tramaR);
                close(packet_socket);
                break;
            case 3:
                return 1;
        }
    }
}

void leerArchivo(char* nombreArchivo){
    FILE *file = fopen(nombreArchivo, "r");
    unsigned char tramaTemp[256];
    int c, banderaTrama = 0, indexT = 0, count = 0;
    if (file == NULL) return;

    while ((c = fgetc(file)) != EOF) {
        if(banderaTrama) {
            if ((char)c == '}') {
                banderaTrama = 0;
                printf("\n%d- Trama a analizar:\n",++count);
                imprimirTrama(tramaTemp,++indexT);
                printf("\n");
                indexT = 0;
                analizarLLC(tramaTemp);
                memset(&tramaTemp[0], 0, sizeof(tramaTemp));
                continue;
            }
            if((char)c != '\n') {
                char tmp;
                fscanf(file, "%02x", &tramaTemp[indexT++]);
            }
        }
        if ((char)c == '{')
            banderaTrama = 1;

    }


    return;
}

void analizarLLC(unsigned char * trama){
    unsigned char MACOrigen[20], MACDestino[20];
    int longitud = trama[12] + trama[13];
    int dsap = trama[14];
    int ssap = trama[15];
    int control1 = trama[16];
    int control2 = trama[17];
    sprintf(MACDestino, "%02x:%02x:%02x:%02x:%02x:%02x",trama[0], trama[1], trama[2], trama[3],
            trama[4],trama[5]);
    sprintf(MACOrigen, "%02x:%02x:%02x:%02x:%02x:%02x",trama[6], trama[7], trama[8], trama[9],
            trama[10],trama[11]);
    printf( "MAC Destino: %s\nMAC Origen: %s\nLongitud: %d\n",MACDestino, MACOrigen, longitud);
    analizarDSAP(dsap);
    analizarSSAP(ssap);
    analizarControl(control1,control2);

}

void analizarControl(int controlByte1, int controlByte2){
    char bitsControl1[8], bitsControl2[8];
    intACadenaBinaria(bitsControl1,controlByte1);
    intACadenaBinaria(bitsControl2,controlByte2);
    printf("Control:\n");
    if(bitsControl1[7] == 0) {
        printf("\tTrama de información\n");
        char bitsTemp[8];
        memcpy(bitsTemp + 1,bitsControl1,7);
        bitsTemp[0] = 0;
        printf("\tEnviado:\t%d\n",cadenaBinariaAInt(bitsTemp));
        memcpy(bitsTemp + 1,bitsControl2,7);
        bitsTemp[0] = 0;
        printf("\tEspera recibir:\t%d",cadenaBinariaAInt(bitsTemp));
        printf(bitsControl2[7] == 0 ?"\n\tComando\n": "\n\tRespuesta\n");
    }
    else if(bitsControl1[6] == 0 && bitsControl1[7] == 1 ) {
        printf("\tTrama de supervisión\n");
        if (bitsControl1[4] == 0 && bitsControl1[5] == 0 )
            printf("\tReceiver Ready\n");
        else if(bitsControl1[4] == 0 && bitsControl1[5] == 1 )
            printf("\tReceiver Not Ready\n");
        else
            printf("\tReject\n");
        char bitsTemp[8];
        memcpy(bitsTemp + 1,bitsControl2,7);
        bitsTemp[0] = 0;
        printf("\tEspera recibir:\t%d",cadenaBinariaAInt(bitsTemp));
        printf(bitsControl2[7] == 0 ?"\n\tComando\n": "\n\tRespuesta\n");
    }

    else {  //Trama no numerada
        printf("\tTrama no numerada\n");
        if (bitsControl1[0] == 0 && bitsControl1[1] == 0 &&
                bitsControl1[2] == 0 && bitsControl1[4] == 1 && bitsControl1[5] == 1) {
            printf("\tDisconnect Mode (DM)\n");
        }
        else if(bitsControl1[0] == 0 && bitsControl1[1] == 1 &&
                bitsControl1[2] == 0 && bitsControl1[4] == 0 && bitsControl1[5] == 0) {
            printf("\tDisconnect (DISC)\n");
        }
        else if(bitsControl1[0] == 0 && bitsControl1[1] == 1 &&
                bitsControl1[2] == 1 && bitsControl1[4] == 0 && bitsControl1[5] == 0) {
            printf("\tUnnumbered Acknowledgment (UA) \n");
        }
        else if(bitsControl1[0] == 0 && bitsControl1[1] == 1 &&
                bitsControl1[2] == 1 && bitsControl1[4] == 1 && bitsControl1[5] == 1) {
            printf("\tSet Asynchronous Balanced Mode (SABME) \n");
        }
        else if(bitsControl1[0] == 1 && bitsControl1[1] == 0 &&
                bitsControl1[2] == 0 && bitsControl1[4] == 0 && bitsControl1[5] == 1) {
            printf("\tFrame Reject (FRMR)\n");
        }
        else if(bitsControl1[0] == 1 && bitsControl1[1] == 0 &&
                bitsControl1[2] == 1 && bitsControl1[4] == 1 && bitsControl1[5] == 1) {
            printf("\tExchange Id (XID)\n");
        }
        else if(bitsControl1[0] == 1 && bitsControl1[1] == 1 &&
                bitsControl1[2] == 1 && bitsControl1[4] == 0 && bitsControl1[5] == 0) {
            printf("\tTest (TEST) \n");
        }
        printf( (bitsControl1[3] == 0)? "\tComando\n": "\tRespuesta\n");
    }

}

void analizarDSAP(int dsap){
    char bitsDsap[8], bitsSAP[8];
    intACadenaBinaria(bitsDsap, dsap);
    memcpy(bitsSAP,bitsDsap,7);
    bitsSAP[7] = 0;
    printf("DSAP: \n\t");
    switchSAP(cadenaBinariaAInt(bitsSAP));
    printf(bitsDsap[7] == 0 ? "\n\tIndividual\n": "\n\tGrupal\n");

}

void intACadenaBinaria(char* buffer, int num){
    for(int i = 0, j = 7; i < 8; i++, j--)
        buffer[j] = (num >> i) & 1;  //Loop para obtener el bit en posición "i"
}

void analizarSSAP(int ssap) {
    char bitsSAP[8], ssapBinario[8];
    intACadenaBinaria(ssapBinario,ssap);
    memcpy(bitsSAP,ssapBinario,7);
    bitsSAP[7] = 0; //Completado de cadena
    printf("SSAP: \n\t");
    switchSAP(cadenaBinariaAInt(bitsSAP));
    printf(ssapBinario[7] == 0 ? "\n\tComando\n": "\n\tRespuesta\n");
}

int cadenaBinariaAInt(char* cadena){
    int multiplier = 0, total = 0;
    for (int i = 7; i >= 0; i-- )
        total += pow(2,multiplier++)* cadena[i];
    return total;
}

void switchSAP(int n){
    switch (n) {
        case 0x00:
            printf("Null LSAP");
            break;
        case 0x02:
            printf("Individual LLC Sublayer Management Function");
            break;
        case 0x03:
            printf("Group LLC Sublayer Management Function");
            break;
        case 0x04:
            printf("IBM SNA Path Control (individual)");
            break;
        case 0x05:
            printf("IBM SNA Path Control (group)");
            break;
        case 0x06:
            printf("ARPANET Internet Protocol (IP)");
            break;
        case 0x08:case 0x34:
        case 0x0C:
            printf("SNA");
            break;
        case 0x0E:
            printf("PROWAY (IEC955) Network Management & Initialization");
            break;
        case 0x18:
            printf("Texas Instruments");
            break;
        case 0x42:
            printf("IEEE 802.1 Bridge Spanning Tree Protocol");
            break;
        case 0x72:
            printf("ISO 8208 (X.25 over IEEE 802.2 Type 2 LLC)");
            break;
        case 0x80:
            printf("Xerox Network Systems (XNS)");
            break;
        case 0x86:
            printf("Nestar");
            break;
        case 0x82:
            printf("PROWAY (IEC 955) Active Station List Maintenance");
            break;
        case 0x98:
            printf("ARPANET Address Resolution Protocol (ARP)");
            break;
        case 0xBC:
            printf("Banyan VINES");
            break;
        case 0xAA:
            printf("SubNetwork Access Protocol (SNAP)");
            break;
        case 0xE0:
            printf("Novell NetWare");
            break;
        case 0xF0:
            printf("IBM NetBIOS");
            break;
        case 0xF4:
            printf("IBM LAN Management (individual)");
            break;
        case 0xF5:
            printf("IBM LAN Management (group)");
            break;
        case 0xF8:
            printf("IBM Remote Program Load (RPL)");
            break;
        case 0xFA:
            printf("Ungermann-Bass");
            break;
        case 0xFE:
            printf("ISO Network Layer Protocol");
            break;
        case 0xFF:
            printf("Global LSAP");
            break;
        default:
            printf("Yikes!");
            break;
    }
}


void imprimirTrama(unsigned char * trama, int tam){
    for (int i = 0; i < tam; i++){
        if( i % 16 == 0)
            printf("\n");
        printf("%.2x ", trama[i]);
    }
    printf("\n");
}

void escuchar(int ds,  int indice, unsigned char* tramaR){
    while(1){
        if ( esLLC(ds,tramaR)){
            printf("Trama LLC detectada!\n");
            analizarLLC(tramaR);
        }
    }
}

bool esLLC(int ds, unsigned char* tramaR){
    int tam;
    if( (tam = recvfrom(ds,tramaR,1514,MSG_DONTWAIT,NULL,0)) != -1 ) {
        return ( (tramaR[12]<<8) + tramaR[13]  < 1500 );
    }
    return false;
}

int obtenerDatos(int ds){
    struct ifreq nic;
    int i, indice;
    char nombre[10];

    printf("Insertar nombre de interfaz\n");
    scanf("%s",nombre);
    strcpy(nic.ifr_name, nombre);

    if(ioctl(ds,SIOCGIFINDEX,&nic) == -1){
        perror("Erro al conseguir indice");
        exit(0);
    }
    return nic.ifr_ifindex;
}
