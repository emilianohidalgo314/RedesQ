#include <sys/socket.h>
#include <linux/if_packet.h>
#include <net/ethernet.h> /* the L2 protocols */
#include <stdlib.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <sys/time.h>
#include <stdbool.h>

#define MAX_HOPS 25
#define MAX_PACKETS 3

int obtenerDatos(int ds);	//descriptor de socket
void estructuraTramaARP(unsigned char *trama);
void estructuraDatagramaIP(unsigned char* trama, int vida, int id);
void enviaTrama(int ds,int index,unsigned char *paq, int lon);
void imprimirTrama(unsigned char* trama, int tam);
void path(int ds, int index, unsigned char* trama);
bool filtroICMP(unsigned char *paq);
void GetGatewayForInterface(const char* interface, char* ipGateway);
bool ipEsLocal();
int recibeTramaARP(int ds, unsigned char* trama);
unsigned short checksum(void *b, int len);
bool vidaAgotada(unsigned char* trama);
bool paqueteRecibido(unsigned char* trama);
int probarLatencia(int ds, int secuencia);

unsigned char datagramaIP[60]={
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x08,0x00,0x45,0x00,
        0x00,0x48,0x00,0x01,0x00,0x00,0x40,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x08,0x00,0x00,0x00,0x00,0x01,0x00,0x01
};

unsigned char tramaARP[60] ={
        0xff,0xff,0xff,0xff,0xff,0xff,0x00,0x00,0x00,0x00,0x00,0x00,0x08,0x06,0x00,0x01,
        0x08,0x00,0x06,0x04,0x00,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,'E','H','C'
};

unsigned char tramaARPResp[60];
char static etherIP[2] = {0x08,0x00};
unsigned char static MACOrigen[6];
unsigned char static MACDestino[6];
unsigned char static ipOrigen[4];
unsigned char static ipDestino[4];
unsigned char static ipGateway[4];
unsigned char mascaraSubred[4];


int main(){
    int packet_socket, indice;
    char ipTemp[25];
    packet_socket = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
    if (packet_socket == -1){
        perror("\nError al abrir el socket");
        exit(0);
    }
    perror("Exito al abrir el socket");
    indice = obtenerDatos(packet_socket);
    printf("\nInsertar IP: ");
    scanf("%s", ipTemp);
    inet_pton(AF_INET, ipTemp, ipDestino);
    if(!ipEsLocal())
        memcpy(ipDestino,ipGateway,4);
    estructuraTramaARP(tramaARP);
    enviaTrama(packet_socket, indice, tramaARP, 60);
    recibeTramaARP(packet_socket, tramaARPResp);
    inet_pton(AF_INET, ipTemp, ipDestino);
    path(packet_socket, indice, datagramaIP);
    close(packet_socket);
    return 1;
}

void path(int ds, int index, unsigned char* trama){
    printf("Hop\tSend/Recived\t\tIP\n");
    for(int i = 1; i <= MAX_HOPS; i++) {
        for (int j = 0; j < MAX_PACKETS; j++) {   //Loop para mandar 4 paquetes con vida "i"
            estructuraDatagramaIP(trama, i, i);
            enviaTrama(ds, index, trama, 60);
        }
        if (probarLatencia(ds, i))
            return;
    }
}

int probarLatencia(int ds, int secuencia){
    struct timeval start, end;
    gettimeofday(&start, NULL);
    long mtime = 0, seconds, useconds;
    int flagPA = 0, paquetesR = 0, tam;//Flag package arrived
    unsigned char tramaR[1516];
    while(mtime < 3000){
        tam = recvfrom(ds,tramaR,1514,MSG_DONTWAIT,NULL,0);
        if (tam !=-1){
            if (filtroICMP(tramaR) && vidaAgotada(tramaR)){
                paquetesR++;
            }
            else if(filtroICMP(tramaR) && paqueteRecibido(tramaR)) {
                paquetesR++;
                flagPA = 1;
            }
        }
        gettimeofday(&end, NULL);
        seconds  = end.tv_sec  - start.tv_sec;
        useconds = end.tv_usec - start.tv_usec;
        mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;
        if (paquetesR == MAX_PACKETS ) break;
    }
    if(paquetesR) {
        printf("%d\t\t%d/%d\t\t", secuencia,paquetesR, MAX_PACKETS);
        for (int k = 0; k < 4; k++)
            printf("%d.",tramaR[26 + k]);
    }
    else
        printf("%d\t\tSin respuesta",secuencia);
    printf("\n\n");
    return flagPA;
}

bool vidaAgotada(unsigned char* trama){
    return trama[34] == 0x0b &&
           trama[35] == 0x00 &&
           !(memcmp(trama + 30, ipOrigen, 4)) &&
           (memcmp(trama + 26, ipOrigen, 4));
}

bool paqueteRecibido(unsigned char* trama){
    return trama[34] == 0x00 &&
           trama[35] == 0x00  &&
           !(memcmp(trama + 26, ipDestino, 4)) &&
           !(memcmp(trama + 30, ipOrigen, 4));
}

void enviaTrama(int ds,int index,unsigned char *paq, int lon){
    int tam;
    struct sockaddr_ll capaEnlace;
    memset(&capaEnlace, 0x00,
           sizeof(capaEnlace));
    capaEnlace.sll_family = AF_PACKET;
    capaEnlace.sll_protocol=htons(ETH_P_ALL);
    capaEnlace.sll_ifindex=index;
    tam=sendto(ds,paq,lon,0,(struct sockaddr*)&capaEnlace,sizeof(capaEnlace));

}

int recibeTramaARP(int ds, unsigned char* trama){
    int tam, flag = 0;
    struct timeval start, end;
    long mtime = 0, seconds, useconds;
    gettimeofday(&start, NULL);
    while(mtime < 200){
        tam = recvfrom(ds,trama,1514,0,NULL,0);
        if (tam !=-1){
            if (!memcmp(trama + 0, MACOrigen,6)){
                printf("Trama ARP recibida!\n");
                imprimirTrama(trama, tam);
                printf("\n");
                flag = 1;
            }
        }
        memcpy(MACDestino, trama + 6, 6);
        gettimeofday(&end, NULL);
        seconds  = end.tv_sec  - start.tv_sec;
        useconds = end.tv_usec - start.tv_usec;
        mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;
        if (flag == 1) break;
    }
    printf("Elapsed time: %ld milliseconds\n", mtime);

}

void estructuraDatagramaIP(unsigned char* trama, int vida, int secuencia){
    char encabezadoIP[33];
    char encabezadoICMP[18];
    //Limpiar Checksum anterior
    memset(trama + 24,0x00, 1);
    memset(trama + 25,0x00, 1);
    memset(trama + 36,0x00, 1);
    memset(trama + 37,0x00, 1);

    //Encabezado MAC
    memcpy(trama+0,MACDestino,6);
    memcpy(trama+6,MACOrigen,6);
    //Encabezado IP
    memset(trama + 17, 0x20, 1);    //Longitud
    memset(trama + 19, 0x01, 1);    //Identificador
    memset(trama + 22, vida, 1);    //Tiempo de vida
    memcpy(trama + 26,ipOrigen,4);
    memcpy(trama + 30,ipDestino,4);
    memset(trama + 40,(unsigned char)(secuencia>>8), 1);
    memset(trama + 41,secuencia & 0xff, 1);

    //Checksum IP
    memcpy(encabezadoIP,trama + 14 ,20);
    int chksumIP = checksum(encabezadoIP,20);
    memset(trama + 24,chksumIP & 0xff, 1);
    memset(trama + 25,(unsigned char)(chksumIP>>8), 1);
    //Checksum IMCP
    memcpy(encabezadoICMP,trama + 34 ,8);
    int chksumIMCP = checksum(encabezadoICMP,8);
    memset(trama + 36,chksumIMCP & 0xff, 1);
    memset(trama + 37,(unsigned char)(chksumIMCP>>8), 1);
}

unsigned short checksum(void *b, int len){
    unsigned short *buf = b;
    unsigned int sum=0;
    unsigned short result;
    for ( sum = 0; len > 1; len -= 2 )
        sum += *buf++;
    if ( len == 1 )
        sum += *(unsigned char*)buf;
    sum = (sum >> 16) + (sum & 0xFFFF);
    sum += (sum >> 16);
    result = ~sum;
    return result;
}

bool filtroICMP(unsigned char *paq){
    return(!memcmp(paq,MACOrigen,6))&&
          (!memcmp(paq + 6,MACDestino,6))&&
          (!memcmp(paq + 12,etherIP,2))&&
          (paq[23] == 0x01);
}

void estructuraTramaARP(unsigned char *trama){
    //Encabezado
    memcpy(trama + 6,MACOrigen,6);
    //Mensaje
    memcpy(trama + 22, MACOrigen,6);
    memcpy(trama + 28,ipOrigen,4);
    memset(trama + 32,0x00,6);
    memcpy(trama + 38,ipDestino,4);
}

void GetGatewayForInterface(const char* interface, char* ipGateway){
    char cmd[1000] = {0x0};
    sprintf(cmd, "route -n | grep %s  | grep 'UG[ \t]' | awk '{print $2}'", interface);
    FILE *fp = popen(cmd, "r");
    char line[256] = {0x0};
    if (fgets(line, sizeof(line), fp) != NULL) {
        line[strcspn(line, "\n")] = 0;
        inet_pton(AF_INET, line, ipGateway);
    }
    pclose(fp);
}


bool ipEsLocal(){
    for (int i = 0; i < 4; i++){
        if (mascaraSubred[i] == 0)  break;
        if(ipOrigen[i] != ipDestino[i]){
            printf("\nIP No local\n");
            return false;
        }
    }
    printf("Local");
    return true;
}

void imprimirTrama(unsigned char * trama, int tam){
    for (int i = 0; i < tam; i++){
        if( i % 16 == 0)	printf("\n");
        printf("%.2x ", trama[i]);
    }
    printf("\n");
}

int obtenerDatos(int ds){
    struct ifreq nic;
    struct sockaddr_in *addr;
    int i, indice;
    char nombre[10];

    printf("Insertar nombre de interfaz: ");
    scanf("%s",nombre);
    strcpy(nic.ifr_name, nombre);
    if(ioctl(ds,SIOCGIFINDEX,&nic) == -1){
        perror("Error al conseguir indice");
        exit(0);
    }

    if(ioctl(ds,SIOCGIFHWADDR,&nic) == -1){
        perror("Error al conseguir MAC");
        exit(1);
    }
    memcpy(MACOrigen, nic.ifr_hwaddr.sa_data + 0, 6);
    printf("La dirección MAC es: ");
    for (i = 0; i < 6; i++)
        printf("%.2X: ", MACOrigen[i]);
    if(ioctl(ds,SIOCGIFADDR,&nic) == -1){
        perror("Error al conseguir IP");
        exit(1);
    }
    memcpy(ipOrigen, nic.ifr_addr.sa_data + 2, 4);
    printf("\nLa IP es: ");
    for (i = 0; i < 4; i++)
        printf("%d.", ipOrigen[i]);

    if(ioctl(ds,SIOCGIFNETMASK,&nic)==-1) {
        perror("\nError al obtener la mascara de subred");
        exit(0);
    }
    else {
        memcpy(mascaraSubred, nic.ifr_netmask.sa_data + 2, 6);
        printf("\nLa mascara de subred es: ");
        for (i = 0; i < 4; i++)
            printf("%d.", mascaraSubred[i]);
        printf("\n");
        GetGatewayForInterface(nombre,ipGateway);
        return nic.ifr_ifindex;
    }
}