#include <sys/socket.h>
#include <linux/if_packet.h>
#include <net/ethernet.h> /* the L2 protocols */
#include <stdlib.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <sys/time.h>
#include <stdbool.h>

int obtenerDatos(int ds);	//descriptor de socket
void estructuraTramaARP(unsigned char *trama);
void enviaTrama(int ds, int indice, unsigned char *trama);
void recibeTrama(int ds, unsigned char* trama);
void imprimirTrama(unsigned char* trama, int tam);
int recibeTramaARP(int ds, unsigned char* trama);
void enviarARP(int ds,int indice, unsigned char* tramaARP, unsigned char* tramaARecibir);
bool esARPGratuito(int ds, unsigned char* tramaR);
bool ipCoincide(int ds,  int indice);
void defenderIP(unsigned char* trama, char* MACDefensor, char* ipDef, int ds, int indice);
void arpGratuito(int ds, int indice, unsigned char* tramaR);

unsigned char MACOrigen[6];
unsigned char MACbro[6] = {0xff,0xff,0xff,0xff,0xff,0xff};  //MAC para broadcast
unsigned char trama[1514], tramaR[1514];    //Trama, tramaRecibida
unsigned char ethertype[2] = {0xaa, 0xaa};
unsigned char ethertypeARP[2] = {0x08,0x06};
unsigned char upCodeRespuesta[2] = {0x00, 0x02};
unsigned char ipOrigen[4];
unsigned char ipDestino[4];

unsigned char tramaARP[60] =
    {
        0xff,0xff,0xff,0xff,0xff,0xff,0x00,0x00,0x00,0x00,0x00,0x00,0x08,0x06,0x00,0x01,
        0x08,0x00,0x06,0x04,0x00,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

int main(){
    int packet_socket, indice;
    char ipTemp[25];

    //Abir socket
    if ( (packet_socket = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL))) == -1 ){
        perror("Woops");
        exit(0);
    }
    perror("Socket abierto con éxito");
    indice = obtenerDatos(packet_socket);
    //enviarARP(packet_socket,  indice, tramaARP,tramaR);
    arpGratuito(packet_socket, indice, tramaR );
    close(packet_socket);
    return 1;
}

void enviarARP(int ds, int indice, unsigned char* tramaARP, unsigned char* tramaARecibir ){
    FILE *f = fopen("registro.txt", "w");
    for (int i = 0; i <= 255; i++){
        memcpy(ipDestino,ipOrigen,3);
        ipDestino[3] = i;
        printf("\n");
        estructuraTramaARP(tramaARP);
        enviaTrama(ds, indice, tramaARP);
        if(recibeTramaARP(ds,tramaARecibir)){
            char ipCadena[30], macCadena[30];
            sprintf(macCadena, "%02x:%02x:%02x:%02x:%02x:%02x",tramaARecibir[6], 
                        tramaARecibir[7], tramaARecibir[8], tramaARecibir[9], 
                        tramaARecibir[10],tramaARecibir[11]);

            memcpy(tramaARecibir + 6, macCadena,6);
            inet_ntop(AF_INET, ipDestino, ipCadena, sizeof(ipCadena) );
            fprintf(f, "%s\t%s\n", ipCadena,macCadena);
        }
    }
    fclose(f);
}

void recibeTrama(int ds, unsigned char* trama){
	int tam, flag = 0;
	
	struct timeval start, end;
    long mtime = 0, seconds, useconds;    
    gettimeofday(&start, NULL);
	
	while(mtime < 1000){
		tam = recvfrom(ds,trama,1514,MSG_DONTWAIT,NULL,0);
		if (tam ==-1){
			perror("Error al recibir");
		}
		else {
			if (!memcmp(trama + 0, MACOrigen,6)){
				printf("Trama recibida!\n");
				imprimirTrama(trama, tam);
				printf("\n");
				flag = 1;
			}	
		}
				gettimeofday(&end, NULL);
				seconds  = end.tv_sec  - start.tv_sec;
				useconds = end.tv_usec - start.tv_usec;
				mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;
			if (flag == 1) break;
	}
	printf("Elapsed time: %ld milliseconds\n", mtime);
}

int recibeTramaARP(int ds, unsigned char* trama){
	int tam, flag = 0;
	struct timeval start, end;
    long mtime = 0, seconds, useconds;    
    gettimeofday(&start, NULL);
	
	while(mtime < 200){
		if( (tam = recvfrom(ds,trama,1514,MSG_DONTWAIT,NULL,0)) != -1 ) {
			if (!memcmp(trama + 0, MACOrigen,6) && 
                !memcmp(trama + 12, ethertypeARP,2) &&
                !memcmp(trama + 28, ipDestino,4) &&
                !memcmp(trama + 20, upCodeRespuesta,2)){
				printf("Trama ARP recibida!\n");
				imprimirTrama(trama, tam);
				printf("\n");
				flag = 1;
			}	
		}
				gettimeofday(&end, NULL);
				seconds  = end.tv_sec  - start.tv_sec;
				useconds = end.tv_usec - start.tv_usec;
				mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;
			if (flag == 1) return 1;
	}
	printf("Elapsed time: %ld milliseconds\n", mtime);
    return 0;
}

void arpGratuito(int ds,  int indice, unsigned char* tramaR){
    while(1){
        if ( esARPGratuito(ds,tramaR)){
            printf("Solicitud ARP recibida!\n");
            ipCoincide(ds, indice);
        }
    }
}

void defenderIP(unsigned char* trama, char* MACDefensor, char* ipDef, int ds, int indice){
    unsigned char macD[6];
    sscanf(MACDefensor, "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx", &macD[0], &macD[1], &macD[2], &macD[3], &macD[4], &macD[5]);
    printf("%s",MACDefensor);
    //  RESPUESTA
    //MAC Infractor
        printf("%d\n",sizeof(tramaARP));

    for(int i = 0; i < 6; i++){
        memcpy(tramaARP + i,tramaR + 6 + i,1);
        memcpy(tramaARP + i + 32, tramaR + 6 + i,1);
    }
        printf("%d\n",sizeof(tramaARP));

    //MAC Def.    
    memcpy(tramaARP + 6, macD, 6);
    memcpy(tramaARP + 22, macD, 6);
    printf("%d\n",sizeof(tramaARP));

    //Mensaje
    memcpy(tramaARP + 28,ipDef, 4);
    memcpy(tramaARP + 38,ipDef,4);
    memset(tramaARP + 21, 0x02, 1);
    imprimirTrama(tramaARP, 60);
    enviaTrama(ds, indice, tramaARP);

    // ARP GRATUITO
    memcpy(trama + 0, MACbro, 6);
    memset(trama + 21, 0x01, 1);
    memset(trama + 32,0x00,6); 
    imprimirTrama(tramaARP, 60);
    enviaTrama(ds, indice, tramaARP);

}
bool ipCoincide(int ds,  int indice){

    FILE * fp;
    char * line = NULL;
    size_t len = 0;
    ssize_t read;
    char ipCadena[30],ipTemp[5], macCadena[30],macDefensor[20];
    memcpy(ipTemp,tramaR + 28, 4);
    inet_ntop(AF_INET, ipTemp, ipCadena, sizeof(ipCadena) ); //IP en formato estándar 
    
    sprintf(macCadena, "%02x:%02x:%02x:%02x:%02x:%02x",
                        tramaR[6], tramaR[7], tramaR[8], tramaR[9], tramaR[10],tramaR[11]);
    
    fp = fopen("registro.txt", "r");
    
    if (fp == NULL)
        exit(EXIT_FAILURE);
    
    while ((read = getline(&line, &len, fp)) != -1) {
        if (!memcmp(line, ipCadena,strlen(ipCadena))){
            if(!memcmp(line + strlen(ipCadena) + 1, macCadena,strlen(macCadena))){
                printf("El IP corresponde con la MAC\n");
            }
            memcpy(macDefensor,line + strlen(ipCadena) + 1, strlen(macCadena));
            defenderIP(tramaARP, macDefensor, ipTemp, ds, indice);
        }
    }
    printf("IP No registrada\n");
    fclose(fp);
    if (line)
        free(line);
    return false;
}

bool esARPGratuito(int ds, unsigned char* trama){
    int tam;
    unsigned char ipARP[4] = {0x00,0x00,0x00,0x00};
    if( (tam = recvfrom(ds,trama,1514,MSG_DONTWAIT,NULL,0)) != -1 ) {
        return  !memcmp(trama + 0, MACbro,6) && 
                !memcmp(trama + 12, ethertypeARP,2) &&
                (!memcmp(trama + 28, trama + 38,4) || !memcmp(trama + 28, ipARP,4) ) ;
    }
    return false;
}

void imprimirTrama(unsigned char * trama, int tam){
    for (int i = 0; i < tam; i++){
        if( i % 16 == 0)	printf("\n");
        printf("%.2x ", trama[i]);
    }
    printf("\n");
}

void estructuraTramaARP(unsigned char *trama){
    //Encabezado
    memcpy(trama + 6,MACOrigen,6);
    
    //Mensaje
    memcpy(trama + 22, MACOrigen, 6);
    memcpy(trama + 28,ipOrigen, 4);
    memset(trama + 32,0x00,6); 
    memcpy(trama + 38,ipDestino,4);
}

void enviaTrama(int ds, int indice, unsigned char *trama){
    int tam;
    struct sockaddr_ll interfaz;
    memset(&interfaz, 0x00, sizeof(interfaz));
    interfaz.sll_family = AF_PACKET;
    interfaz.sll_protocol = htons(ETH_P_ALL);
    interfaz.sll_ifindex = indice;
    tam = sendto(ds,trama,60,0,(struct sockaddr *)&interfaz,sizeof(interfaz));
    if(tam == - 1 ){
        perror("Error al mandar paquete");
        exit(2);
    }
    printf("Paquete enviado con éxito\n");
}

int obtenerDatos(int ds){
    struct ifreq nic;
    int i, indice;
    char nombre[10];

    printf("Insertar nombre de interfaz\n");
    scanf("%s",nombre);
    strcpy(nic.ifr_name, nombre);

    if(ioctl(ds,SIOCGIFINDEX,&nic) == -1){
        perror("Erro al conseguir indice");
        exit(0);
    }

    if(ioctl(ds,SIOCGIFHWADDR,&nic) == -1){
        perror("Erro al conseguir MAC");
        exit(1);
    }

    memcpy(MACOrigen, nic.ifr_hwaddr.sa_data + 0, 6);
    printf("La dirección MAC es\n");
    for (i = 0; i < 6; i++)
        printf(" %.2X: ", MACOrigen[i]);

    if(ioctl(ds,SIOCGIFADDR,&nic) == -1){
        perror("Erro al conseguir IP");
        exit(1);
    }

    memcpy(ipOrigen, nic.ifr_addr.sa_data + 2, 4);
    printf("\nLa IP es\n");
    for (i = 0; i < 4; i++)
        printf("%d.", ipOrigen[i]);
    printf("\n");

    return nic.ifr_ifindex;
}